export const API_URL = "https://pixel.w84.vkforms.ru/HappySanta/slaves/1.0.0";
export const REQ_DELAY = 1000;
export const AXIOS_HEADERS = {
  accept: "application/json, text/plain, */*",
  "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
  "accept-encoding": "gzip, deflate, br",
  "sec-ch-ua":
    '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
  "sec-ch-ua-mobile": "?0",
  "sec-fetch-dest": "empty",
  "sec-fetch-mode": "cors",
  "sec-fetch-site": "cross-site",
  referrer: "https://prod-app7794757-c1ffb3285f12.pages-ac.vk-apps.com/",
  origin: "https://prod-app7794757-c1ffb3285f12.pages-ac.vk-apps.com/",
  authority: "pixel.w84.vkforms.ru",
  "user-agent":
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36",
};
export const INTERVAL_MS = 1000 * 60;
export const ACTION_DELAY = 1500;
