import {
  BuyFetterResponse,
  BuyResponse,
  GetResponse,
  SetJobResponse,
} from "../interface/slave";
import { SlaveData } from "../interface/slave-data";
import { getSlaveName } from "../utils";
import { apiGet, apiPost } from "./sender";

export class Slave {
  private readonly id: number;
  private readonly token: string;
  public data: SlaveData;

  public chain = true;
  public work = true;

  constructor(id: number, token: string, data?: SlaveData) {
    this.id = id;
    this.token = token;

    if (data) {
      this.data = data;
      return;
    }

    this.data = {
      id: 0,
      job: "",
      profit: 0,
      sale_price: 0,
      fetter_price: 0,
      fetter_to: new Date(0),
    };
  }

  public get clearProfit(): number {
    const clearProfit = this.data.profit - this.data.fetter_price / 120;
    return clearProfit;
  }

  public async get(chain = true): Promise<GetResponse> {
    const slaveInfo = (await apiGet(`user?id=${this.id}`, this.token))
      .data as GetResponse;

    this.data.id = slaveInfo.id;
    this.data.job = slaveInfo.job.name;
    this.data.profit = slaveInfo.profit_per_min;
    this.data.sale_price = slaveInfo.sale_price;
    this.data.fetter_price = slaveInfo.fetter_price;
    this.data.fetter_to = new Date(slaveInfo.fetter_to * 1000);

    if (chain && this.chain === true && this.data.fetter_to < new Date()) {
      console.log("Ставлю цепь");
      await this.buyFetter();
    }

    return slaveInfo;
  }

  public async buyFetter(): Promise<BuyFetterResponse> {
    const data = (await apiPost("buyFetter", this.token, { slave_id: this.id }))
      .data as BuyFetterResponse;
    this.data.id = data.id;
    this.data.job = data.job.name;
    this.data.profit = data.profit_per_min;
    this.data.sale_price = data.sale_price;
    this.data.fetter_price = data.fetter_price;
    this.data.fetter_to = new Date(data.fetter_to * 1000);
    console.log(`Куплена цепь на @id${this.data.id}
Работа: ${this.data.job}
Доход: ${this.data.profit}
Стоимость цепи: ${this.data.fetter_price}
Можно продать за ${this.data.sale_price}`);
    return data;
  }

  public async buy(): Promise<BuyResponse> {
    const data = (await apiPost("buySlave", this.token, { slave_id: this.id }))
      .data;
    await this.get();
    console.log(`Куплен раб @id${this.data.id}
Работа: ${this.data.job}
Доход: ${this.data.profit}
Стоимость цепи: ${this.data.fetter_price}
Можно продать за ${this.data.fetter_price}`);
    return data;
  }

  public async upgradeFull() {
    while (this.data.fetter_price < 35000) {
      await apiPost("saleSlave", this.token, { slave_id: this.id });

      const data = (
        await apiPost("buySlave", this.token, { slave_id: this.id })
      ).data;

      await this.get(false);
      console.log(`Цепи на раба стоят: ${this.data.fetter_price}`);
    }

    console.log(`Раб @id${this.id} улучшен!`);
  }

  public async setJob(work?: string): Promise<SetJobResponse> {
    work = work ?? getSlaveName();

    const data = (
      await apiPost("jobSlave", this.token, {
        slave_id: this.id,
        name: work,
      })
    ).data.slave as SetJobResponse;
    this.data.id = data.id;
    this.data.job = data.job.name;
    this.data.profit = data.profit_per_min;
    this.data.sale_price = data.sale_price;
    this.data.fetter_price = data.fetter_price;
    this.data.fetter_to = new Date(data.fetter_to * 1000);
    console.log(`Установлена новая работа для @id${this.data.id}
Работа: ${this.data.job}
Новая работа: ${work}
Доход: ${this.data.profit}
Стоимость цепи: ${this.data.fetter_price}
Можно продать за ${this.data.fetter_price}`);
    return data;
  }
}
