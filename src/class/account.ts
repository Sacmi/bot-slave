import { ACTION_DELAY } from "../constants";
import { AccountInfoResponse, AccountParams } from "../interface/account";
import { checkId, sleep } from "../utils";
import { apiGet } from "./sender";
import { Slave } from "./slave";

export default class Account {
  private token: string;
  private params: AccountParams;

  public slaves: Slave[] = [];
  public info: {
    balance: number;
    profit: number;
    clearProfit: number;
    reservMoney: number;
  } = {
    balance: 0,
    profit: 0,
    clearProfit: 0,
    reservMoney: 0,
  };

  constructor(
    token: string,
    params: AccountParams = {
      chain: true,
      work: true,
    }
  ) {
    this.token = token;
    this.params = params;
  }

  public async updateInfo(): Promise<AccountInfoResponse> {
    const accountInfo = (await apiGet("start", this.token))
      .data as AccountInfoResponse;

    this.slaves = accountInfo.slaves.map((slave) => {
      const tempSlave = new Slave(slave.id, this.token, {
        id: slave.id,
        job: slave.job.name,
        profit: slave.profit_per_min,
        sale_price: slave.sale_price,
        fetter_price: slave.fetter_price,
        fetter_to: new Date(slave.fetter_to * 1000),
      });

      tempSlave.chain = this.params.chain;
      tempSlave.work = this.params.work;

      return tempSlave;
    });

    this.info.balance = accountInfo.me.balance;
    this.info.profit = accountInfo.me.slaves_profit_per_min;
    this.info.clearProfit = this.slaves
      .map((slave) => slave.clearProfit)
      .reduce((accumulator, currentValue) => accumulator + currentValue);

    this.info.reservMoney = this.slaves
      .map((slave) => slave.data.fetter_price)
      .reduce((accumulator, currentValue) => accumulator + currentValue);

    console.log(`Обновил данные в ${new Date().toLocaleString()}
Баланс: ${this.info.balance}
Доход: ${this.info.profit}
Чистый доход: ${this.info.clearProfit}
Рабов: ${this.slaves.length}
Резерв: ${this.info.reservMoney}

Резерв заполнится примерно через ${Math.floor(
      (this.info.reservMoney - this.info.balance) / this.info.clearProfit
    )} минут`);
    return accountInfo;
  }

  public async checkSlaves() {
    for (const slave of this.slaves) {
      if (slave.data.profit < 1000) {
        console.log(`@id${slave.data.id} надо проапгрейдить!`);
        await slave.upgradeFull();
      }
      if (
        slave.chain === true &&
        slave.data.fetter_to < new Date() &&
        (slave.data.fetter_price < 100000 || checkId(slave.data.id))
      ) {
        console.log(`Цепи закончились на @id${slave.data.id}`);
        await slave.buyFetter();
        await sleep(ACTION_DELAY);
      }
      if (slave.work === true && slave.data.job === "") {
        console.log(`@id${slave.data.id} нихуя не делает. Фиксим`);
        await slave.setJob(process.env.DEFAULT_NAME);
        await sleep(ACTION_DELAY);
      }
    }
  }
}
